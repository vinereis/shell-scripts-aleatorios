#!/bin/bash
###############################
# Script de Backup de um Servidor Local para um Externo
# Data: 19/10/2021
# VersÃ£o: 0.1
#
###############################

diretorio_origem=/home/smc
diretorio_destino=usuario@192.168.0.12:/home/smc/backups/
log=/script_backup_smc/log_script/date +%d%m%Y.txt

# Copia o ultimo arquivo gerado na origem para o destino

echo "Inicio da transferencia" >> $log
echo "DIA" date +%d/%m/%Y >> $log
echo "HORA" date +%H:%M:%S >> $log

# Entra no diretorio Origem dos arquivos

cd $diretorio_origem

# Copia os arquivos para o Servidor Externo

scp ls -t | grep .zip | head -n1 $diretorio_destino

echo ###########################
echo "Finalizacao da transferencia" >> $log
echo "DIA" date +%d/%m/%Y >> $log
echo "HORA" date +%H:%M:%S >> $log

# Envia o log por email

mail -r "Assunto <email@dominio>" -v -s "Entra em contato comigo" emaildeenvio@dominio -q "anexo"
