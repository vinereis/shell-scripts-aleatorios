#!/bin/bash --norc
#
# Simples aviso sobre o alto consumo de memoria RAM.
#
# Obs.:
# - Este script e executavel somente em modo grafico;
# - Para executa-lo, basta inicializa-lo juntamente com o X ou 
# simplesmente executa-lo em segundo-plano;
# - Ao pressionar o botao 'Fechar' da janela que aparecera, o script
# sera finalizado por completo. Para reativa-lo reinicie a sessao ou
# execute-o novamente em segundo-plano.

# Sinal 'kill' recebido:
trap "break 2 ; exit" 1 2 3 15

# Executavel somente em modo grafico:
[[ -z $DISPLAY ]] && echo "Script executavel somente em modo grafico." && exit 1

# Porcentagem maxima de uso da memoria, antes de emitir aviso:
pct=80
# Tempo (em segundos) para checagem do uso da memoria:
tcm=30
# Tempo (em segundos) de intervalo entre as emissoes dos avisos:
int=10
# Tempo (em segundos) de duracao do aviso na tela:
dur=8

# Determinando valores
_Valores_() {
   # Quantidade de memoria RAM no sistema:
   local -i tram=$(awk '{print $2}' <(sed -u '2!d' <(free -m)))
   # Quantidade de memoria utilizada pelo sistema:
   local -i ramu=$(awk '{print $3}' <(sed -u '2!d' <(free -m)))
   # Porcentagem de memoria utilizada:
   pmu=$((ramu * 100 / tram))
   return $pmu
}

# 1 - Loop de checagem de uso da memoria:
while sleep ${tcm}s
do
   # 2 - Loop de teste e execucao:
   while :
   do
      # Chamando funcao:
      _Valores_      
      # Comparando valores:
      if [ $pmu -gt $pct ]
      then
         # Aviso ao usuario:
         xmessage -buttons Ok:0,Fechar:2 \
         -title "Aviso" -timeout $dur -center \
"Atenção: Há pouca memória RAM disponível no momento.
Faça 'top -u \$USER' e finalize os processos desnecessários
para poder liberar mais memória e evitar lentidão no sistema."

         # Botao 'Fechar' foi pressionado?
         [[ $? == 2 ]] && break 2
         
         # Intervalo para emissao do proximo aviso:
         sleep ${int}s
         continue
      else
         # Refazendo checagem: 
         break
      fi
   done
done
exit
# Fim
