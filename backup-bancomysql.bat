@echo off

for /f "tokens=1-4 delims=/ " %%a IN ('DATE /T') do (set MYDATE=%%a%%b%%c%%d)
for /f "tokens=1-2 delims=: " %%a in ('TIME /T') do (set MYTIME=%%ah%%bm)

SET MYSQL_PATH="C:\ProgramFiles\MySQL\MySQL Server 5.6\"
SET MYSQL_USER=user
SET MYSQL_PASS=pass
SET LOG_PATH="E:\Backup\Log\"

SET MYSQL_HOST=localhost
SET MYSQL_PORT=3306
SET NOME_ARQUIVO=backup.%MYDATE%.%MYTIME%.sql

SET ARQUIVO="E:\Backup\Base\%NOME_ARQUIVO%"

SET MYSQL_DATABASE=base

@echo Iniciando o Script de Backup da Base em %MYDATE%-%MYTIME% >> %LOG_PATH%\log.txt
@echo .

%MYSQL_PATH%\bin\mysqldump.exe -v --host=%MYSQL_HOST% --user=%MYSQL_USER% --password=%MYSQL_PASS% --port=%MYSQL_PORT% --protocol=tcp --force --allow-keywords --compress --add-drop-table --default-character-set=latin1 --hex-blob --result-file=%ARQUIVO% %MYSQL_DATABASE%

@echo.

@echo Compactando o Arquivo...
@echo.
@echo |TIME /T
@echo.
"C:\Program Files\Winrar\Winrar.exe" a -r -ibck -inul "E:\Backup\Compactado\%NOME_ARQUIVO%.RAR" "E:\Backup\Base\*.sql"
@echo.
@echo Excluindo arquivo sql que foi compactado...
del "E:\Backup\Base\*.sql"

@echo.
@echo |TIME /T
@echo.

FORFILES /S /p E:\Backup\Compactado\ /d -15 /M *.rar /c "CMD /C DEL @FILE /Q"

@echo Finalizando o Backup da Base em %MYDATE%-%MYTIME% >> %LOG_PATH%\log.txt
@echo.
